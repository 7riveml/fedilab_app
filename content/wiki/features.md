*[Home](../home) &gt;*   Features
<div align="center">
<img src="../res/fedilab_icon.png" width="70px">
<b>Fedilab</b>
</div>

## Features
Here is the list of some features included in Fedilab:

* [Reorder timelines](./reorder-timelines)
* [Follow Twitter timelines with Nitter](./twitter-timelines-with-nitter)
* [Live notifications](./live-notifications)
* [Invidious and Nitter](./invidious-nitter)
* [Cross-account actions](cross-account-actions)
* [Schedule toots](./schedule-toots)
* [Custom themes](./custom-themes)
* [Export/Import data](./export-import-data)
* [Custom emojis](./custom-emojis)
* [Changeable launcher icons](./change-icons)
* [Filter toots](./filter-toots)
* [Share URLs](./share-urls)
* [Art timeline](./art-timeline)
* [Archive statuses](./archive-statuses)
* [Custom sharing](./custom-sharing)
<br><br>
