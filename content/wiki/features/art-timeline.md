*[Home](../../home) > [Features](../../features) &gt;* Art timeline

<div align="center">
<img src="../../res/fedilab_icon.png" width="70px">
<b>Fedilab</b>
</div>

## Art Timeline
Art Timeline is a unique feature in Fedilab. It takes media from toots with 'mastoart' tag and displays them in a separate timeline

- Art timeline can be turned off in settings

- Click on an image to view it in full screen

- Click on the author's name to open the original toot

- Long press on art timeline icon to change the visibility of NSFW content. It's disabled by default<br><br><img src="../../res/art/timeline.png" width="300px">
